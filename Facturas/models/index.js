const dbCofig = require("../config/dbConfig");
const Sequelize = require("sequelize");

const sequelize = new Sequelize(dbCofig.db, dbCofig.user, dbCofig.password,{
  host: dbCofig.host,
  dialect: dbCofig.dialect,
  operatorAliases: false,

  pool: {
    max: dbCofig.pool.max,
    min: dbCofig.pool.min,
    acquire: dbCofig.pool.acquire,
    idle: dbCofig.pool.idle
  }
});

const db = {};

db.Sequelize = Sequelize;
db.sequelize = sequelize;

db.clients = require("./client.js")(sequelize, Sequelize);

module.exports = db;