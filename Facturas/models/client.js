module.exports = (sequelize, Sequelize) => {
  const Client = sequelize.define("client",{
    dni:{
      type: Sequelize.STRING(9)
    },
    name:{
      type: Sequelize.STRING
    },
    lastName:{
      type: Sequelize.STRING
    },
    address:{
      type: Sequelize.STRING
    },
    location:{
      type: Sequelize.STRING
    },
    responsible:{
      type: Sequelize.STRING
    }
  });

  return Client;
};