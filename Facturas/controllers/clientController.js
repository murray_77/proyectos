const db = require("../models");
const Client = db.clients;
const Op = db.Sequelize.Op;


exports.newClient = async (req, res) =>{
  if (!req.body.name||!req.body.lastName||!req.body.dni){
    res.status(400).send({
      message: "Campo vacío"
    });
    return;
  }

  const client = {
    dni: req.body.dni,
    name: req.body.name,
    lastName: req.body.lastName,
    address: req.body.address,
    location: req.body.location,
    responsible: req.body.responsible
  }

  let data = await Client.create(client);
  
  try{
    res.send(data);
  }catch(err){
    res.status(500).send({
      message: err.message || "Error al crear."
    });
  }
  
}

exports.getClient = async(req, res) =>{
  const id = req.params.id;
  
  const data = await Client.findByPk(id);

  try{
    res.send(data);
  }catch(err){
    res.status(500).send({
      message: err.message || "Error al buscar al cliente con id="+id
    });
  }
}

exports.getAllClients = async(req, res) =>{
  const name = req.query.name;
  var condition = name ? {name: {[Op.like]: `%${name}%`}} : null;

  const data = await Client.findAll({where: condition})

  try{
    res.send(data);
  }catch(err){
    res.status(500).send({
      message: err.message || "Error al buscar al cliente(s)"
    });
  }
}

exports.updateClient = async(req, res) =>{
  const id = req.params.id;

  const data = await Client.update(req.body, {where: {id: id}});

  try{
    if(data == 1){
      res.send({
        message: "Cliente modificado con éxito"
      });
    }else{
      res.send({
        message: "Error al actualizar el cliente, (No existe?)"
      });
    }
  }catch(err){
    res.status(500).send({
      message: err.message || "Error al actualizar el cliente"
    });
  }  
}

exports.deleteClient = async(req, res) =>{
  const id = req.params.id;

  const data = await Client.destroy({where: {id: id}});
  try{
    if(data == 1){
      res.send({
        message: "Cliente eliminado con éxito"
      });
    }else{
      res.send({
        message: "Error al eliminar el cliente, (No existe?)"
      });
    }
  }catch(err){
    res.status(500).send({
      message: err.message || "Error al eliminar el cliente"
    });
  }

}