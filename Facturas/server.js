const express = require("express");
const bodyParser = require("body-parser");
const cors = require("cors");
const db = require("./models");


const app = express();

var corsOptions = {
  origin: true 
};

app.use(cors(corsOptions));

//parsear peticiones content-type - aplication/json
app.use(bodyParser.json());

//parsear peticiones content-type - aplication/x-www-form-urlencoded
app.use(bodyParser.urlencoded({extended:true}));

db.sequelize.sync({alter: true});
require("./routes/clientRouter")(app);

//ruta inicial
app.get("/", (req, res) => {
  res.json({message: "Página inicial"});
});

//establecer puerto peticiones
const PORT = process.env.PORT || 8080;
app.listen(PORT,() => {
  console.log("Server funcionando en el puerto "+PORT);
})