import React, {Component} from "react";
import { Link, Route, Switch } from "react-router-dom";
import NewClient from "./components/newClient";
import AllClients from "./components/allClients";
import Client from "./components/client";
import './App.css';

class App extends Component{
  render(){
    return(
      <div>
        <nav className="navbar">
          <a href="/clients" className="inicio">Inicio</a>
          <div>
            <li><Link to={"/clients"}>Clientes</Link></li>
            <li><Link to={"/add"}>Nuevo</Link></li>
          </div>
        </nav>

        <div>
          <Switch>
            <Route exact path={["/","/clients"]} component={AllClients}/>
            <Route exact path="/add" component={NewClient}/>
            <Route path="/clients/:id" component={Client}/>
          </Switch>
        </div>
      </div>
    );
  }
}

export default App;
