import React, {Component} from "react";
import ClientService from "../services/clientService";

export default class NewClient extends Component{
  constructor(props){
    super(props);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeDni = this.onChangeDni.bind(this);
    this.onChangeLastName = this.onChangeLastName.bind(this);
    this.onChangeAddress = this.onChangeAddress.bind(this);
    this.onChangeLocation = this.onChangeLocation.bind(this);
    this.onChangeResponsible = this.onChangeResponsible.bind(this);
    this.saveClient = this.saveClient.bind(this);
    this.newClient = this.newClient.bind(this);
    this.isMinor = this.isMinor.bind(this);
  
    this.state = {
      id: null,
      dni: "",
      name: "",
      lastName: "",
      address: "",
      location: "",
      responsible: "",     
      isMinor: false,
      disabled: true
    }
  }

  componentDidMount(){
    console.log(this.state);
  }

  onChangeDni = (e) =>{
    this.setState({
      dni: e.target.value
    });
  }

  onChangeName = (e) =>{
    this.setState({
      name: e.target.value
    });
  }

  onChangeLastName = (e) =>{
    this.setState({
      lastName: e.target.value
    })
  }

  onChangeAddress = (e) =>{
    this.setState({
      address: e.target.value
    })
  }

  onChangeLocation = (e) =>{
    this.setState({
      location: e.target.value
    })
  }

  onChangeResponsible = (e) =>{

    this.setState({
      responsible: e.target.value
    })
  }

  isMinor = (e) =>{
    if(e.target.checked){
      this.setState({disabled:false});
    }else{
      this.setState({disabled:true});
    }
  }

  saveClient = async() =>{
    let data = this.state;
    delete data.disabled;
    delete data.isMinor;

    try{
      await ClientService.create(data);
      alert("Cliente añadido");
      this.newClient();
    }catch(err){
      console.log(err);
    }
  }

  newClient = () =>{
    this.setState({
      newClient:{
        id: null,
        dni: "",
        name: "",
        lastName: "",
        address: "",
        location: "",
        responsible: ""
      },      
      isMinor: false,
      disabled: true
    })
  }

  render(){
    const {disabled, required} = this.state;
    return(
      <div>
        <form name="formulario">
          <div>
            <label htmlFor="dni">DNI</label>
            <input type="text" id="dni" required value={this.state.dni} onChange={this.onChangeDni} name="dni"/>
          </div>

          <div>
            <label htmlFor="name">Nombre</label>
            <input type="text" id="name" required value={this.state.name} onChange={this.onChangeName} name="name"/>
          </div>  

          <div>
            <label htmlFor="lastName">Apellidos</label>
            <input type="text" id="lastName" required value={this.state.lastName} onChange={this.onChangeLastName} name="lastName"/>
          </div>

          <div>
            <label htmlFor="address">Dirección</label>
            <input type="text" id="address" required value={this.state.address} onChange={this.onChangeAddress} name="address"/>
          </div>

          <div>
            <label htmlFor="location">Concello</label>
            <input type="text" id="location" value={this.state.location} onChange={this.onChangeLocation} name="location" required/>
          </div>

          <div>
            <label>Menor de edad</label>
            <input type="checkbox" id="minor" name="minor" onChange={this.isMinor}/>
          </div>

          <div>
            <label htmlFor="responsible">Responsable Legal</label>
            <input type="text" id="responsible" value={this.state.responsible} onChange={this.onChangeResponsible} name="responsible" required={required} disabled={disabled}/>
          </div>
        </form>
        <button onClick={this.saveClient}>Aceptar</button>

      </div>
    );
  }

  
}