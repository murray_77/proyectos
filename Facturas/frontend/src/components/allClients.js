import React, {Component} from "react";
import clientService from "../services/clientService";
import {Link} from "react-router-dom";

export default class AllClients extends Component{
  constructor(props){
    super(props);
    this.getClients = this.getClients.bind(this);
    this.reload = this.reload.bind(this);
    this.deleteClient = this.deleteClient.bind(this);
    this.setActiveClient = this.setActiveClient.bind(this);
    //this.searchByName.bind(this);
  
    this.state = {
      clients: [],
      activeClient: null,
      currentIndex: -1,
      searchClient: ""
    }
  }

  componentDidMount = () =>{
    this.getClients();
  }

  getClients = async() =>{
    let data;
    
    try{
      data = await clientService.getAll();
      this.setState({clients: data.data});
    }catch(err){
      console.log(err);
    }    
  }

  deleteClient = async(client) =>{
    // eslint-disable-next-line no-restricted-globals
    var del = confirm("Deseas eliminar a "+client.name+"?")

    if(del){
      try{
        await clientService.delete(client.id);
        alert("Entrada eliminada");
        this.reload();
      }catch(err){
        console.log(err);
      }      
    }
  }

  reload = () =>{
    this.getClients();
    this.setState({
      activeClient: null,
      currentIndex: -1
    })
  }

  setActiveClient = (client, index) =>{
    this.setState({
      activeClient: client,
      currentIndex: index
    })
  }


  render(){
    
    const {clients, activeClient, /*currentIndex*/} = this.state;

    return(
      <div>
        <div>
          <input type="text" placeholder="Buscar Cliente"/>
          <button type="button" >Buscar</button>
        </div>

        <div>
          <h4>Lista de clientes</h4>
          <ul>
            {clients && clients.map((client, index)=>(
              <li key={index} onClick={()=>this.setActiveClient(client, index)}>
                {client.name+" "+client.lastName}
              </li>
            ))}
          </ul>
        </div>
        
        <div>
          {activeClient ? (
            <div>
              <h4>Cliente</h4>
              <div>
                <label>DNI</label>
                {activeClient.dni}
              </div>
              <div>
                <label>Nombre</label>
                {activeClient.name+" "+activeClient.lastName}
              </div>
              <Link to={"/clients/"+activeClient.id}>Editar</Link>
              <button type="button" onClick={()=>this.deleteClient(activeClient)}>Eliminar</button>
            </div>
          ) : (
            <div>
              <p>Selecciona cliente</p>
            </div>
          )}
        </div>
      </div>
    );
  }
}