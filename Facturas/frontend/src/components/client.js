import React, {Component} from "react";
import clientService from "../services/clientService";

export default class Client extends Component{
  constructor(props){
    super(props);
    this.onChangeDni = this.onChangeDni.bind(this);
    this.onChangeName = this.onChangeName.bind(this);
    this.onChangeLastName = this.onChangeLastName.bind(this);
    this.getClient = this.getClient.bind(this);
    this.updateClient = this.updateClient.bind(this);
    this.deleteClient = this.deleteClient.bind(this);

    this.state = {
      activeClient: {
        id: null,
        dni: "",
        name: "",
        lastName: ""
      },
      message: ""
    }      
  }

  componentDidMount = () =>{
    this.getClient(this.props.match.params.id);
  }

  getClient = async(id) =>{
    let data;
    console.log(id);
    try{
      data = await clientService.getById(id);
      this.setState({
        activeClient: data.data
      })
    }catch(err){
      console.log(err)
    }
  }

  onChangeName = (e) =>{
    const name = e.target.value;
    this.setState(function(prevState){
      return{
        activeClient:{
          ...prevState.activeClient,
          name: name
        }
      };
    });
  }

  onChangeDni = (e) =>{
    const dni = e.target.value;
    this.setState(function(prevState){
      return{
        activeClient:{
          ...prevState.activeClient,
          dni: dni
        }
      };
    });
  }

  onChangeLastName = (e) =>{
    const lastName = e.target.value;
    this.setState(function(prevState){
      return{
        activeClient:{
          ...prevState.activeClient,
          lastName: lastName
        }
      };
    });
  }

  updateClient = () =>{
    let response;

    try{
      response = clientService.update(this.state.activeClient.id, this.state.activeClient);
      console.log(response.data);
      this.setState({message: "Editado con éxito"});
    }catch(err){
      console.log(err);
    }
  }


  deleteClient = async() =>{
    // eslint-disable-next-line no-restricted-globals
    var del = confirm("Deseas eliminar a "+this.state.activeClient.name+"?")

    if(del){
      try{
        await clientService.delete(this.state.activeClient.id);
        alert("Entrada eliminada");
        this.props.history.push('/allClients')
      }catch(err){
        console.log(err);
      }      
    }
  }

  render(){

    const {activeClient} = this.state;

    return(
      <div>
        {activeClient ? (
          <div>
            <h4>Cliente</h4>
            <form>
              <div>
                <label htmlFor="dni"></label>
                <input type="text" id="dni" value={activeClient.dni} onChange={this.onChangeDni}/>
              </div>
              <div>
                <label htmlFor="name"></label>
                <input type="text" id="name" value={activeClient.name} onChange={this.onChangeName}/>
              </div>
              <div>
                <label htmlFor="lastName"></label>
                <input type="text" id="lastName" value={activeClient.lastName} onChange={this.onChangeLastName}/>
              </div>
            </form>

            <button onClick={this.deleteClient}>Eliminar</button>
            <button onClick={this.updateClient}>Confirmar</button>
            <p>{this.state.message}</p>
          </div>
        ) : (
          <div>
            <p>No hay cliente activo?</p>
          </div>
        )}
      </div>
    );
  }
}