module.exports = app =>{
  const clients = require("../controllers/clientController");

  var router = require("express").Router();

  //Nuevo cliente
  router.post("/", clients.newClient);

  //listar todos (o por nombre)
  router.get("/", clients.getAllClients);

  //Buscar por id
  router.get("/:id", clients.getClient);

  //Modificar cliente
  router.put("/:id", clients.updateClient);

  //Borrar cliente
  router.delete("/:id", clients.deleteClient);

  app.use("/api/clients", router);
}